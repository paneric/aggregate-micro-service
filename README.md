# AGGREGATE MICRO SERVICE CONSOLE

## Clone repository

```sh
$ git clone https://paneric@bitbucket.org/paneric/aggregate-micro-service.git
$ mv aggregate-micro-service e-commerce-user-address
$ cd e-commerce-user-address
$ rm -r -f .git
```

## Set a custom module

Adapt project `name` and `psr-4`:

*composer.json*
```json
{
    "name": "paneric/e-commerce-user-address",
    ...
    "autoload": {
        "psr-4": {
            "ECommerce\\UserAddress\\": "src"
        }
    },
    ...
}
```

Run composer:

```sh
$ composer update
$ composer require paneric/e-commerce-user
$ composer require paneric/e-commerce-address
```

Make sure the `scope` variable is set as `lib`:

*bin/app*
```php
#!/usr/bin/env php
<?php

$scope = 'lib';
...
```
Run console command:

```sh
$ bin/app ams

Psr-4 (no trailing slash !!!): ECommerce
Service vendor folder path (relative to project folder, no trailing slash !!!): vendor/paneric/e-commerce-user-address
Service name (CamelCase !!!): UserAddress
Service prefix (lower case !!!): ua
Related services vendor folders paths (coma separated, relative to project folder, no trailing slash !!!): vendor/paneric/e-commerce-user,vendor/paneric/e-commerce-address
Related services names (CamelCase, coma separated !!!): User,Address
Related services prefixes (lower case, coma separated !!!): usr,adr
Other services names (CamelCase, coma separated !!!): ListCountry,ListTypeCompany,Credential
DB table name: user_addresss

                                                                          
  AGGREGATE MICRO SERVICE UPDATE SUCCESS:                                  
                                                                          

  Resources update with vendor "ECommerce", service "UserAddress" and prefix "ua" success.
```

## Manual update

*src/UserAddressApi/config/dependencies/repository.php*  
*src/UserAddressApp/config/dependencies/repository.php*
```php
return [
    'user_address_repository' => static function (Container $container): UserAddressRepository
    {
        return new UserAddressRepository(
            $container->get(Manager::class),
            [
                'table' => 'user_addresss',//{aggregate_table}
                'dao_class' => UserAddressDAO::class,
                'create_unique_where' => sprintf(
                    ' %s',
                    'WHERE ua_address_id=:ua_address_id'
                ),
                'update_unique_where' => sprintf(
                    ' %s',
                    'WHERE ua_address_id=:ua_address_id'
                ),
            ]
        );
    },

    'user_address_query' => static function (Container $container): UserAddressQuery
    {
        return new UserAddressQuery(
            $container->get(Manager::class),
            [
                'adao_class' => UserAddressADAO::class,
                'base_query' => '
                    SELECT * FROM user_addresss bt
                        JOIN users st1 ON bt.ua_user_id = st1.usr_id
                            JOIN credentials st1_1 ON st1.usr_credential_id = st1_1.crd_id
                        JOIN addresss st2 ON bt.ua_address_id = st2.adr_id
                            JOIN list_countrys st2_1 ON st2.adr_list_country_id = st2_1.lc_id
                            JOIN list_type_companys st2_2 ON st2.adr_list_type_company_id = st2_2.ltc_id                
                ',
            ]
        );
    },
];
```

## Environment variables
*.env*
```
ENV = 'dev'
APP_DOMAIN = '127.0.0.1:8080'
BASE_API_URL = 'http://127.0.0.1:8081'

# GUARD:
KEY_ASCII = 'def000008d147968b763eac2453624272c423c77347bd423e736328d648365fb243ea2130953f36ff38b5c9d8b15e5063df9e014010a3e06a1ec60612e54737a9f54f45d'
# DB:
DB_HOST = 'localhost'
DB_NAME = 'e_commerce'
DB_USR = 'toor'
DB_PSWD = 'toor'
JWT_SECRET = 'NRA4kcEMGUr+w5zOWpsVJ7v4NmQVVAuMZfZFll5g4pjnTGHJFj8Fw24bvUlRsttJrY0/ZBtav66nCzuD0S/rrBuNRGsV8QkulREVa9krRxTO/mnx1fgVIPBMacHMm6hN'
```

## Apache

Create log folder:
```sh
$ mkdir var
$ mkdir var/cache
$ mkdir var/logs
$ sudo chmod -R 777 var
```

*/etc/apache2/sites-available*
```sh
$ cp 000-default.conf e-commerce.conf
```

*/etc/apache2/sites-available/e-commerce.conf*
```
Listen 8080
Listen 8081

<VirtualHost 127.0.0.1:8080>
    ServerAdmin webmaster@e-commerce.dev
    ServerName e-commerce-user-address.dev
    ServerAlias www.e-commerce-user-address.dev

    DocumentRoot /home/paneric/Documents/php-dev/E-COMMERCE/e-commerce-user-address
    
    <Directory /home/paneric/Documents/php-dev/E-COMMERCE/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>

    ErrorLog /home/paneric/Documents/php-dev/E-COMMERCE/e-commerce-user-address/var/apache_error.log
    CustomLog /home/paneric/Documents/php-dev/E-COMMERCE/e-commerce-user-address/var/access.log combined
</VirtualHost>

<VirtualHost 127.0.0.1:8081>
    ServerAdmin webmaster@e-commerce.dev
    ServerName e-commerce-user-address.dev
    ServerAlias www.e-commerce-user-address.dev

    DocumentRoot /home/paneric/Documents/php-dev/E-COMMERCE/e-commerce-user-address
    
    <Directory /home/paneric/Documents/php-dev/E-COMMERCE/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>

    ErrorLog /home/paneric/Documents/php-dev/E-COMMERCE/e-commerce-user-address/var/apache_error.log
    CustomLog /home/paneric/Documents/php-dev/E-COMMERCE/e-commerce-user-address/var/access.log combined
</VirtualHost>
```

*/etc/apache2/sites-available*
```
$ sudo a2ensite e-commerce.conf  
$ sudo a2dissite 000-default.conf
```


Ensure that the Apache mod_rewrite module is installed and enabled. In order to enable mod_rewrite you can type the following command in the terminal:
```sh
$ sudo a2enmod rewrite
$ sudo a2enmod actions
```

Restart Apache:
```sh
$ sudo systemctl restart apache2  
$ systemctl status apache2.service
$ sudo apachectl configtest
```

*e-commerce-user-address/.htaccess*
```
RewriteEngine On
RewriteCond %{REQUEST_URI} !^/public
RewriteRule ^(.*)$ /public/$1 [NC,L]

RewriteRule .* - [env=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
```


*e-commerce-user-address/public/.htaccess*
```
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^ index.php [QSA,L]

RewriteRule .* - [env=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
```

